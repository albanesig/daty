import React,{Component} from 'react';
import './Daty.scss';
import {Col, Row} from "reactstrap";
import DragAndDrop from "../DragAndDrop/DragAndDrop";
import {blobToSHA512} from "file-to-sha512";

class Daty extends Component {

  constructor(props){
    super(props);
    this.state = {
      files: []
    }
  }

  encryptSHA512 = async (file) => {
    return await blobToSHA512(file)
  };

  handleDrop = (files) => {
    console.log("handleDrop: ", files);
    let fileList = [...this.state.files];
    for (let i = 0; i < files.length; i++) {
      if (!files[i]) return;
      this.encryptSHA512(files[i]).then(hash => {
        fileList.push({
          fileHash: hash
        })
      })
    }
    this.setState({files: fileList})
  };

  render() {

    // console.log("daty state ", this.state.files);

    return <div className="Daty">
      <Col lg={5} className="Box">
        <Row>
          <Col className="Register" lg={6}>REGISTER</Col>
          <Col className="Verify" lg={6}>VERIFY</Col>
        </Row>
        <Row>
          <Col>
            <DragAndDrop handleDrop={this.handleDrop}>
              <div style={{height: 100, width: 100, border: '3px solid #8bc7cf'}} />
            </DragAndDrop>
          </Col>
        </Row>
        <Row>
          <div className="AddDoc">DROP HERE
            <i className="fas fa-level-up-alt fa-lg ml-1" />
          </div>
        </Row>
        <Row>
          <Col lg={12} className="HashesContainer">
            {
              this.state.files && this.state.files.length > 0 ?
              this.state.files.map((file, index) =>
              <div className="InlineHash" key={index}>
                <span>
                {file.fileHash}
                </span>
              </div>
            ) : <div>No available documents</div>
            }
          </Col>
        </Row>
      </Col>
    </div>
  }
}

export default Daty