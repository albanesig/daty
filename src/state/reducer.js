import * as actionTypes from './action';
import {combineReducers} from 'redux';
import {reducer as form} from 'redux-form';

const initialState = {
  files: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_FILES: {
      console.log(action.files);
      return {
        ...state,
        files: action.files
      }
    }
    default:
      return state;
  }
};

export default combineReducers({
  form,
  reducer
})