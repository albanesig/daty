import React from 'react'
import {Col, Row} from "reactstrap";
import Daty from "../../components/Daty/Daty";
import './DatyContainer.scss';

const DatyContainer = () => {

  return <div>
    <Row>
      <Col lg={12} className="DatyContainer">
          <Daty/>
      </Col>
    </Row>
    <Row>
      <Col lg={12} className="Footer">
        Footer
      </Col>
    </Row>
  </div>
};

export default DatyContainer;