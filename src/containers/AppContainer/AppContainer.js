import React from 'react';
import DatyContainer from "../DatyContainer/DatyContainer";

const AppContainer = () => {

  return <div>
    <DatyContainer />
  </div>
};

export default AppContainer